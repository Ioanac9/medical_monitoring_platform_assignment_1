import React from 'react';
import './App.css';

import { BrowserRouter as Router, Route, Switch,Link } from 'react-router-dom'
import ListUserComponent from "./component/user/ListUserComponent";
import AddUserComponent from "./component/user/AddUserComponent";
import EditUserComponent from "./component/user/EditUserComponent";
import ListPatientComponent from "./component/patient/ListPatientComponent";
import AddPatientComponent from "./component/patient/AddPatientComponent";
import EditPatientComponent from "./component/patient/EditPatientComponent";
import EditPatientViewComponent from "./component/patient/EditPatientViewComponent";
import ListCaregiverComponent from "./component/caregiver/ListCaregiverComponent";
import AddCaregiverComponent from "./component/caregiver/AddCaregiverComponent";
import EditCaregiverComponent from "./component/caregiver/EditCaregiverComponent";
import ListMedicationComponent from "./component/medication/ListMedicationComponent";
import AddMedicationComponent from "./component/medication/AddMedicationComponent";
import EditMedicationComponent from "./component/medication/EditMedicationComponent";
import DoctorComponent from "./component/doctor/DoctorComponent";
import LoginFormComponent from "./component/login/LoginFormComponent";
import ValidateLoginComponent from "./component/login/ValidateLoginComponent";
import CaregiverView from "./component/caregiverView/CaregiverView";
import CaregiverComponent from "./component/caregiverView/CaregiverComponent";
import AddPatientToCaregiverComponent from "./component/patient/AddPatientToCaregiverComponent";
import Check from "./component/patient/Check";
import AddMedicationPlanComponent from "./component/medicationPlan/AddMedicationPlanComponent";
import ListPrescriptionComponent from "./component/medicationPlan/ListPrescriptionComponent";
import AddPrescriptionComponent from "./component/medicationPlan/AddPrescriptionComponent";
import CaregiverViewComponent from "./component/caregiverView/CaregiverViewComponent";
import PatientViewComponent from "./component/patientView/PatientViewComponent";


function App() {
    return (
        <div className="container">
            <Router>
                <div className="container">

                    <nav className="navbar navbar-expand-lg navheader">

                        <div className="collapse navbar-collapse" >

                            <ul className="navbar-nav mr-auto">

                                <li className="nav-item">

                                    <Link to={'/login'} className="nav-link">Login</Link>

                                </li>

                            </ul>

                        </div>

                    </nav> <br />
                    <Switch>
                        <Route exact path='/login' component={LoginFormComponent} />
                        <Route path="/validateLogin" component={ValidateLoginComponent} />
                        <Route path="/caregiverView" component={CaregiverView} />
                        <Route path="/doctors" component={DoctorComponent} />

                        <Route path="/" exact component={ListPatientComponent} />
                        <Route path="/users" component={ListUserComponent} />
                        <Route path="/patients" component={ListPatientComponent} />
                        <Route path="/caregivers" component={ListCaregiverComponent} />
                        <Route path="/medications" component={ListMedicationComponent} />
                        <Route path="/add-user" component={AddUserComponent} />
                        <Route path="/add-patient" component={AddPatientComponent} />
                        <Route path="/add-patient" component={AddPatientComponent} />
                        <Route path={"/edit-patient-view"}component = {EditPatientViewComponent}/>
                        {/*<Route path="/login" component={LoginFormComponent} />*/}

                        <Route path="/one-caregiver" component={CaregiverComponent} />





                        <Route path="/add-patient-to-caregiver"component={AddPatientToCaregiverComponent}/>
                        <Route path="/check"component={Check}/>

                        <Route path="/add-medication-plan" component ={AddMedicationPlanComponent}/>
                        <Route path="/list-prescription-plan" component ={ListPrescriptionComponent}/>
                        <Route path="/add-prescription" component ={AddPrescriptionComponent}/>

                        <Route path="/caregiver-view" component ={CaregiverViewComponent}/>
                        <Route path="/patient-view" component ={PatientViewComponent}/>

                        <Route path="/add-caregiver" component={AddCaregiverComponent} />
                        <Route path="/add-medication" component={AddMedicationComponent} />
                        <Route path="/edit-user" component={EditUserComponent} />
                        <Route path="/edit-patient" component={EditPatientComponent} />
                        <Route path="/edit-caregiver" component={EditCaregiverComponent} />
                        <Route path="/edit-medication" component={EditMedicationComponent} />
                    </Switch>

                </div>

            </Router>
        </div>
    );
}

const style = {
    color: 'red',
    margin: '10px'
}

export default App;
