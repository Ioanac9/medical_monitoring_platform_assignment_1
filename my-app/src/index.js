import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';

// import "./styles.css";
// import ValidatedLoginForm from "./ValidatedLoginForm";
//
// function App() {
//     return (
//         <div className="App">
//             <h1>Validated Login Form</h1>
//             <ValidatedLoginForm />
//         </div>
//     );
// }

ReactDOM.render(<App />, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
// serviceWorker.unregister();
