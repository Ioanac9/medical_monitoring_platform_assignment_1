import React, { Component } from 'react'
import ApiServiceCaregiver from "../../service/ApiServiceCaregiver";
import ApiServicePatient from "../../service/ApiServicePatient";
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Button from '@material-ui/core/Button';
import CreateIcon from '@material-ui/icons/Create';
import DeleteIcon from '@material-ui/icons/Delete';
import IconButton from '@material-ui/core/IconButton';

import Typography from '@material-ui/core/Typography';
import ApiServiceMedication from "../../service/ApiServiceMedication";
import ApiServiceMedicationPlan from "../../service/ApiServiceMedicationPlan";
import ApiServiceLogin from "../../service/ApiServiceLogin";

class DoctorComponent extends Component {


    constructor(props) {
        super(props)
        this.state = {
            caregivers: [],
            patients: [],
            medications: [],
            firstName: '',
            lastName: '',
            birthDate: '',
            gender: '',
            address: '',
            medicalRecord: '',
            Caregiver_idCaregiver: '',
            message: null
        }
        this.deleteCaregiver = this.deleteCaregiver.bind(this);
        this.editCaregiver = this.editCaregiver.bind(this);
        this.addCaregiver = this.addCaregiver.bind(this);
        this.reloadCaregiverList = this.reloadCaregiverList.bind(this);
        this.deletePatient = this.deletePatient.bind(this);
        this.editPatient = this.editPatient.bind(this);
        this.addPatient = this.addPatient.bind(this);
        this.reloadPatientList = this.reloadPatientList.bind(this);
        this.savePatient = this.savePatient.bind(this);
        this.deleteMedication = this.deleteMedication.bind(this);
        this.editMedication = this.editMedication.bind(this);
        this.addMedication = this.addMedication.bind(this);
        this.reloadMedicationList = this.reloadMedicationList.bind(this);
      //  this.createMedicationPlanForPatient = this.createMedicationPlanForPatient(this);
    }

    savePatient = (e) => {
        e.preventDefault();
        let patient = {firstName: this.state.firstName, lastName: this.state.lastName, birthDate: this.state.birthDate, gender: this.state.gender, address: this.state.address, medicalRecord: this.state.medicalRecord,Caregiver_idCaregiver: this.state.Caregiver_idCaregiver};
        ApiServicePatient.addPatient(patient)
            .then(res => {
                this.setState({message : 'Patient added successfully.'});
                this.props.history.push('/patients');
            });
    }
    componentDidMount() {
        this.reloadCaregiverList();
        this.reloadPatientList();
        this.reloadMedicationList();
    }

    reloadMedicationList() {
        ApiServiceMedication.fetchMedications()
            .then((res) => {
                this.setState({medications: res.data.result})
            });
    }

    deleteMedication(medicationId) {
        ApiServiceMedication.deleteMedication(medicationId)
            .then(res => {
                this.setState({message : 'Medication deleted successfully.'});
                this.setState({medications: this.state.medications.filter(medication => medication.id !== medicationId)});
            })
    }

    editMedication(id) {
        window.localStorage.setItem("medicationId", id);
        this.props.history.push('/edit-medication');
    }

    addMedication() {
        window.localStorage.removeItem("medicationId");
        this.props.history.push('/add-medication');
    }
    reloadCaregiverList() {
        ApiServiceCaregiver.fetchCaregivers()
            .then((res) => {
                this.setState({caregivers: res.data.result})
            });
    }

    reloadPatientList() {
        ApiServicePatient.fetchPatients()
            .then((res) => {
                this.setState({patients: res.data.result})
            });
    }

    deleteCaregiver(caregiverId) {
        ApiServiceCaregiver.deleteCaregiver(caregiverId)
            .then(res => {
                this.setState({message : 'Caregiver deleted successfully.'});
                this.setState({caregivers: this.state.caregivers.filter(caregiver => caregiver.id !== caregiverId)});
            })
    }

    deletePatient(patientId) {
        ApiServicePatient.deletePatient(patientId)
            .then(res => {
                this.setState({message : 'Patient deleted successfully.'});
                this.setState({patients: this.state.patients.filter(patient => patient.id !== patientId)});
                this.reloadCaregiverList();
            })
    }
    createMedicationPlanForPatient(patientId){
        window.localStorage.setItem("medicationPlanPatientId", patientId);
        this.props.history.push('/list-prescription-plan');
    }

    editCaregiver(id) {
        window.localStorage.setItem("caregiverId", id);
        this.props.history.push('/edit-caregiver');
    }

    editPatient(id) {
        window.localStorage.setItem("patientId", id);
        this.props.history.push('/edit-patient');
    }

    addCaregiver() {
        window.localStorage.removeItem("caregiverId");
        this.props.history.push('/add-caregiver');
    }

    addPatient() {
        window.localStorage.removeItem("patientId");
        this.props.history.push('/add-patient');
    }
    addPatientToCaregiver(caregiverId){
        window.localStorage.setItem("patientToCaregiverId", caregiverId);
        this.props.history.push('/add-patient-to-caregiver');
    }

    render() {
        return (
            <div>
                <br></br>
                <Typography variant="h4" style={style}>Caregiver Details</Typography>
                <Button variant="contained" color="primary" onClick={() => this.addCaregiver()}>
                    Add Caregiver
                </Button>

                <Table>
                    <TableHead>
                        <TableRow>
                            <TableCell align = "right" >Id</TableCell>
                            <TableCell align = "right" >First Name</TableCell>
                            <TableCell align = "right" >Last Name</TableCell>
                            <TableCell align="right">Birth Date</TableCell>
                            <TableCell align="right">Gender</TableCell>
                            <TableCell align="right">Address</TableCell>
                            <TableCell align="right">Patients</TableCell>

                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {this.state.caregivers.map(row => (
                            <TableRow key={row.id}>
                                <TableCell component="th" scope="row">
                                    {row.id}
                                </TableCell>
                                <TableCell align="right">{row.firstName}</TableCell>
                                <TableCell align="right">{row.lastName}</TableCell>
                                <TableCell align="right">{row.birthDate}</TableCell>
                                <TableCell align="right">{row.gender}</TableCell>
                                <TableCell align="right">{row.address}</TableCell>
                                <TableCell align="right">
                                    <TableRow>
                                        <TableCell align = "right" >Id</TableCell>
                                        <TableCell align = "right" >First Name</TableCell>
                                        <TableCell align = "right" >Last Name</TableCell>
                                        <TableCell align="right">Birth Date</TableCell>
                                        <TableCell align="right">Gender</TableCell>
                                        <TableCell align="right">Address</TableCell>
                                        <TableCell align="right">Medical Records</TableCell>

                                    </TableRow>
                                    {row.patients.map(row => {
                                    return (
                                        <TableRow align = "right" key={row.id}>
                                            <TableCell component="th" scope="row">
                                                {row.id}
                                            </TableCell>
                                            <TableCell align="right">{row.firstName}</TableCell>
                                            <TableCell align="right">{row.lastName}</TableCell>
                                            <TableCell align="right">{row.birthDate}</TableCell>
                                            <TableCell align="right">{row.gender}</TableCell>
                                            <TableCell align="right">{row.address}</TableCell>
                                            <TableCell align="right">{row.medicalRecord}</TableCell>

                                            <TableCell align="right"
                                                       onClick={() => this.editPatient(row.id)}><CreateIcon/></TableCell>
                                            <TableCell align="right"
                                                       onClick={() => this.deletePatient(row.id)}><DeleteIcon/></TableCell>

                                        </TableRow>
                                    )})}
                                    <Button variant="contained" color="primary" onClick={() => this.addPatientToCaregiver(row.id)}>
                                        Add Patient
                                    </Button>

                                </TableCell>


                                <TableCell align="right" onClick={() => this.editCaregiver(row.id)}><CreateIcon /></TableCell>
                                <TableCell align="right" onClick={() => this.deleteCaregiver(row.id)}><DeleteIcon /></TableCell>


                            </TableRow>
                        ))}


                    </TableBody>
                </Table>
                <br></br>
                <Typography variant="h4" style={style}>Patient Details</Typography>

                <Table>
                    <TableHead>
                        <TableRow>
                            <TableCell>Id</TableCell>
                            <TableCell>First Name</TableCell>
                            <TableCell align="right">Last Date</TableCell>
                            <TableCell align="right">Birth Date</TableCell>
                            <TableCell align="right">Gender</TableCell>
                            <TableCell align="right">Address</TableCell>
                            <TableCell align="right">Medical Record</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {this.state.patients.map(row => (
                            <TableRow key={row.id}>
                                <TableCell component="th" scope="row">
                                    {row.id}
                                </TableCell>
                                <TableCell align="right">{row.firstName}</TableCell>
                                <TableCell align="right">{row.lastName}</TableCell>
                                <TableCell align="right">{row.birthDate}</TableCell>
                                <TableCell align="right">{row.gender}</TableCell>
                                <TableCell align="right">{row.address}</TableCell>
                                <TableCell align="right">{row.medicalRecord}</TableCell>
                                <TableCell align="right" onClick={() => this.editPatient(row.id)}><CreateIcon /></TableCell>
                                <TableCell align="right" onClick={() => this.deletePatient(row.id)}><DeleteIcon /></TableCell>
                                <Button variant="contained" color="primary" onClick={() => this.createMedicationPlanForPatient(row.id)}>
                                    Create Plan
                                </Button>



                            </TableRow>
                        ))}
                    </TableBody>
                </Table>

                <br></br>
                <Typography variant="h4" style={style}>Medication Details</Typography>
                <Button variant="contained" color="primary" onClick={() => this.addMedication()}>
                    Add Medication
                </Button>

                <Table>
                    <TableHead>
                        <TableRow>
                            <TableCell>Id</TableCell>
                            <TableCell>Name</TableCell>
                            <TableCell align="right">Side Effects</TableCell>
                            <TableCell align="right">Dosage</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {this.state.medications.map(row => (
                            <TableRow key={row.id}>
                                <TableCell component="th" scope="row">
                                    {row.id}
                                </TableCell>
                                <TableCell align="right">{row.name}</TableCell>
                                <TableCell align="right">{row.sideEffects}</TableCell>
                                <TableCell align="right">{row.dosage}</TableCell>

                                <TableCell align="right" onClick={() => this.editMedication(row.id)}><CreateIcon /></TableCell>
                                <TableCell align="right" onClick={() => this.deleteMedication(row.id)}><DeleteIcon /></TableCell>

                            </TableRow>
                        ))}
                    </TableBody>
                </Table>


            </div>
        );
    }

}

const style ={
    display: 'flex',
    justifyContent: 'center'
}

export default DoctorComponent;