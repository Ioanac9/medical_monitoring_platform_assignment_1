import React, { Component } from 'react'
import ApiServiceCaregiver from "../../service/ApiServiceCaregiver";
import ApiServicePatient from "../../service/ApiServicePatient";
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Button from '@material-ui/core/Button';
import CreateIcon from '@material-ui/icons/Create';
import DeleteIcon from '@material-ui/icons/Delete';
import SvgIcon from '@material-ui/core/SvgIcon';

import Typography from '@material-ui/core/Typography';

class ListCaregiverComponent extends Component {


    constructor(props) {
        super(props)
        this.state = {
            caregiver: '',
            caregivers: [],
            patients: [],
            firstName: '',
            lastName: '',
            birthDate: '',
            gender: '',
            address: '',
            medicalRecord: '',
            Caregiver_idCaregiver: '',
            message: null
        }
        this.deleteCaregiver = this.deleteCaregiver.bind(this);
        this.editCaregiver = this.editCaregiver.bind(this);
        this.addCaregiver = this.addCaregiver.bind(this);
        this.reloadCaregiverList = this.reloadCaregiverList.bind(this);
        this.deletePatient = this.deletePatient.bind(this);
        this.editPatient = this.editPatient.bind(this);
        this.addPatient = this.addPatient.bind(this);
        this.reloadPatientList = this.reloadPatientList.bind(this);
        this.savePatient = this.savePatient.bind(this);
    }

    savePatient = (e) => {
        e.preventDefault();
        let patient = {firstName: this.state.firstName, lastName: this.state.lastName, birthDate: this.state.birthDate, gender: this.state.gender, address: this.state.address, medicalRecord: this.state.medicalRecord,Caregiver_idCaregiver: this.state.Caregiver_idCaregiver};
        ApiServicePatient.addPatient(patient)
            .then(res => {
                this.setState({message : 'Patient added successfully.'});
                this.props.history.push('/patients');
            });
    }
    componentDidMount() {
        this.reloadCaregiverList();
        this.reloadPatientList();
    }

    reloadCaregiverList() {


     ApiServiceCaregiver.fetchCaregivers()
                .then((res) => {
                    this.setState({caregivers: res.data.result})
                });
    }

    reloadCaregiver(id){

        ApiServiceCaregiver.fetchCaregiverById(id)
            .then((res) => {
                this.setState({caregiver: res.data.result})
            });

             window.localStorage.setItem("caregiverId", id);
             this.props.history.push('/one-caregiver');

    }

    reloadPatientList() {
        ApiServicePatient.fetchPatients()
            .then((res) => {
                this.setState({patients: res.data.result})
            });
    }

    deleteCaregiver(caregiverId) {
        ApiServiceCaregiver.deleteCaregiver(caregiverId)
            .then(res => {
                this.setState({message : 'Caregiver deleted successfully.'});
                this.setState({caregivers: this.state.caregivers.filter(caregiver => caregiver.id !== caregiverId)});
            })
    }

    deletePatient(patientId) {
        ApiServicePatient.deletePatient(patientId)
            .then(res => {
                this.setState({message : 'Patient deleted successfully.'});
                this.setState({patients: this.state.patients.filter(patient => patient.id !== patientId)});
                this.reloadCaregiverList();
            })
    }

    editCaregiver(id) {
        window.localStorage.setItem("caregiverId", id);
        this.props.history.push('/edit-caregiver');
    }

    editPatient(id) {
        window.localStorage.setItem("patientId", id);
        this.props.history.push('/edit-patient');
    }

    addCaregiver() {
        window.localStorage.removeItem("caregiverId");
        this.props.history.push('/add-caregiver');
    }

    addPatient(caregiverId) {
        window.localStorage.removeItem("patientId");
        window.localStorage.setItem("Caregiver_idCaregiver", caregiverId);
        this.props.history.push('/add-patient/',caregiverId);
    }

    render() {
        return (
            <div>
                <Typography variant="h4" style={style}>Caregiver Details</Typography>
                <Button variant="contained" color="primary" onClick={() => this.addCaregiver()}>
                    Add Caregiver
                </Button>

                <Table>
                    <TableHead>
                        <TableRow>
                            <TableCell align = "right" >Id</TableCell>
                            <TableCell align = "right" >First Name</TableCell>
                            <TableCell align = "right" >Last Name</TableCell>
                            <TableCell align="right">Last Date</TableCell>
                            <TableCell align="right">Birth Date</TableCell>
                            <TableCell align="right">Gender</TableCell>
                            <TableCell align="right">Address</TableCell>
                            <TableCell align="right">Patients</TableCell>

                        </TableRow>
                    </TableHead>
                    <TableBody>

                             {this.state.caregivers.map(row => (
                                                         <TableRow key={row.id}>
                                                             <TableCell component="th" scope="row">
                                                                 {row.id}
                                                             </TableCell>
                                                             <TableCell align="right">{row.firstName}</TableCell>
                                                             <TableCell align="right">{row.lastName}</TableCell>
                                                             <TableCell align="right">{row.birthDate}</TableCell>
                                                             <TableCell align="right">{row.gender}</TableCell>
                                                             <TableCell align="right">{row.address}</TableCell>
                                                             <TableCell align="right">{row.patients.map(row => {
                                                                 return (
                                                                     <TableCell  align = "right" key={row.id}>
                                                                         <TableCell component="th" scope="row">
                                                                             {row.id}
                                                                         </TableCell>
                                                                         <TableCell align="right">{row.firstName}</TableCell>
                                                                         <TableCell align="right">{row.lastName}</TableCell>
                                                                         <TableCell align="right">{row.birthDate}</TableCell>
                                                                         <TableCell align="right">{row.gender}</TableCell>
                                                                         <TableCell align="right">{row.address}</TableCell>


                                                                         <TableCell align="right"
                                                                                    onClick={() => this.editPatient(row.id)}><CreateIcon/></TableCell>
                                                                         <TableCell align="right"
                                                                                    onClick={() => this.deletePatient(row.id)}><DeleteIcon/></TableCell>

                                                                     </TableCell>
                                                                 )})}
                                                                 <Button variant="contained" color="primary" onClick={() => this.addPatient(row.id)}>
                                                                     Add Patient
                                                                 </Button>

                                                             </TableCell>



                                                             <TableCell align="right" onClick={() => this.editCaregiver(row.id)}><CreateIcon /></TableCell>
                                                             <TableCell align="right" onClick={() => this.deleteCaregiver(row.id)}><DeleteIcon /></TableCell>

                                                                <Button variant="contained" color="primary" onClick={() => this.reloadCaregiver(row.id)}>
                                                                    Reload
                                                                 </Button>


                                                         </TableRow>
                                                     ))}



                    </TableBody>
                </Table>

            </div>
        );
    }

}

const style ={
    display: 'flex',
    justifyContent: 'center'
}

export default ListCaregiverComponent;