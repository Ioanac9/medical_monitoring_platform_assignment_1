
import { FormGroup, FormControl, FormLabel } from "react-bootstrap";
import "./Login.css";
import React, { Component } from 'react'
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import ApiServiceLogin from "../../service/ApiServiceLogin";
import Typography from '@material-ui/core/Typography';
import Radio from "@material-ui/core/Radio";
import ApiServicePatient from "../../service/ApiServicePatient";


    class LoginFormComponent extends Component {


        constructor(props) {
            super(props)


            this.state = {
                id: '',
                username: '',
                password: '',
                role: '',
                submitted: false,
                loading: false,
                error: '',
                message: null
            };

        }


        componentDidMount() {
        }

        loginUser(username,password) {


            window.localStorage.setItem("usernameLogin", username);
            window.localStorage.setItem("passwordLogin", password);

            this.props.history.push('/validateLogin');


        }


            onChange = (e) =>
                this.setState({[e.target.name]: e.target.value});


            render()
            {
                return (
                    <div >
                        <form onSubmit>
                            <FormGroup controlId="username" bsSize="large">
                                <FormLabel>Username</FormLabel>

                                <TextField type="text" placeholder="Username" fullWidth margin="normal" name="username" value={this.state.username} onChange={this.onChange}/>

                                </FormGroup>
                            <FormGroup controlId="password" bsSize="large">
                                <FormLabel>Password</FormLabel>
                                <TextField type="password" placeholder="Password" fullWidth margin="normal" name="password" value={this.state.password} onChange={this.onChange}/>
                            </FormGroup>

                            <Button variant="contained" color="primary" onClick={() => this.loginUser(this.state.username,this.state.password)}>
                                Login
                            </Button>


                        </form>
                    </div>
                );

            }
        }

export default LoginFormComponent;