import React, { Component } from 'react'
import ApiServiceMedication from "../../service/ApiServiceMedication";
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Button from '@material-ui/core/Button';
import CreateIcon from '@material-ui/icons/Create';
import DeleteIcon from '@material-ui/icons/Delete';
import Typography from '@material-ui/core/Typography';

class ListMedicationComponent extends Component {

    constructor(props) {
        super(props)
        this.state = {
            medications: [],
            message: null
        }
        this.deleteMedication = this.deleteMedication.bind(this);
        this.editMedication = this.editMedication.bind(this);
        this.addMedication = this.addMedication.bind(this);
        this.reloadMedicationList = this.reloadMedicationList.bind(this);
    }

    componentDidMount() {
        this.reloadMedicationList();
    }

    reloadMedicationList() {
        ApiServiceMedication.fetchMedications()
            .then((res) => {
                this.setState({medications: res.data.result})
            });
    }

    deleteMedication(medicationId) {
        ApiServiceMedication.deleteMedication(medicationId)
            .then(res => {
                this.setState({message : 'Medication deleted successfully.'});
                this.setState({medications: this.state.medications.filter(medication => medication.id !== medicationId)});
            })
    }

    editMedication(id) {
        window.localStorage.setItem("medicationId", id);
        this.props.history.push('/edit-medication');
    }

    addMedication() {
        window.localStorage.removeItem("medicationId");
        this.props.history.push('/add-medication');
    }

    render() {
        return (
            <div>
                <Typography variant="h4" style={style}>Medication Details</Typography>
                <Button variant="contained" color="primary" onClick={() => this.addMedication()}>
                    Add Medication
                </Button>

                <Table>
                    <TableHead>
                        <TableRow>
                            <TableCell>Id</TableCell>
                            <TableCell>Name</TableCell>
                            <TableCell align="right">Side Effects</TableCell>
                            <TableCell align="right">Dosage</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {this.state.medications.map(row => (
                            <TableRow key={row.id}>
                                <TableCell component="th" scope="row">
                                    {row.id}
                                </TableCell>
                                <TableCell align="right">{row.name}</TableCell>
                                <TableCell align="right">{row.sideEffects}</TableCell>
                                <TableCell align="right">{row.dosage}</TableCell>

                                <TableCell align="right" onClick={() => this.editMedication(row.id)}><CreateIcon /></TableCell>
                                <TableCell align="right" onClick={() => this.deleteMedication(row.id)}><DeleteIcon /></TableCell>

                            </TableRow>
                        ))}
                    </TableBody>
                </Table>

            </div>
        );
    }

}

const style ={
    display: 'flex',
    justifyContent: 'center'
}

export default ListMedicationComponent;