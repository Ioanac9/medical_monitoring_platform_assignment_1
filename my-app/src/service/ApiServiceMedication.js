import axios from 'axios';

const MEDICATION_API_BASE_URL = 'http://localhost:8080/medication';

class ApiServiceMedication {

    fetchMedications() {
        return axios.get(MEDICATION_API_BASE_URL);
    }

    fetchMedicationById(medicationId) {
        return axios.get(MEDICATION_API_BASE_URL + '/' + medicationId);
    }

    deleteMedication(medicationId) {
        return axios.delete(MEDICATION_API_BASE_URL + '/' + medicationId);
    }

    addMedication(medication) {
        return axios.post(""+MEDICATION_API_BASE_URL, medication);
    }

    editMedication(medication) {
        return axios.put(MEDICATION_API_BASE_URL + '/' + medication.id, medication);
    }

}

export default new ApiServiceMedication();