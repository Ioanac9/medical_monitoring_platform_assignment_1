import axios from 'axios';

const MEDICATIONPLAN_API_BASE_URL = 'http://localhost:8080/medPlan';

class ApiServiceMedicationPlan {

    fetchMedicationPlans() {
        return axios.get(MEDICATIONPLAN_API_BASE_URL);
    }

    fetchMedicationPlanById(medicationPlanId) {
        return axios.get(MEDICATIONPLAN_API_BASE_URL + '/' + medicationPlanId);
    }

    deleteMedicationPlan(medicationPlanId) {
        return axios.delete(MEDICATIONPLAN_API_BASE_URL + '/' + medicationPlanId);
    }

    addMedicationPlan(medicationPlan) {
        return axios.post(""+MEDICATIONPLAN_API_BASE_URL, medicationPlan);
    }

    editMedicationPlan(medicationPlan) {
        return axios.put(MEDICATIONPLAN_API_BASE_URL + '/' + medicationPlan.id, medicationPlan);
    }

}

export default new ApiServiceMedicationPlan();