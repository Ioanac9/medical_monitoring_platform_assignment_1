import axios from 'axios';

const CAREGIVER_API_BASE_URL = 'http://localhost:8080/caregiver';

class ApiService {

    fetchCaregivers() {
        return axios.get(CAREGIVER_API_BASE_URL);
    }

    fetchCaregiverById(caregiverId) {
        return axios.get(CAREGIVER_API_BASE_URL + '/' + caregiverId);
    }

    deleteCaregiver(caregiverId) {
        return axios.delete(CAREGIVER_API_BASE_URL + '/' + caregiverId);
    }

    addCaregiver(caregiver) {
        return axios.post(""+CAREGIVER_API_BASE_URL, caregiver);
    }

    editCaregiver(caregiver) {
        return axios.put(CAREGIVER_API_BASE_URL + '/' + caregiver.id, caregiver);
    }

}

export default new ApiService();