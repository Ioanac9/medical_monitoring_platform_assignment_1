package com.devglan.controller;

import com.devglan.model.ApiResponse;
import com.devglan.enitites.Patient;
import com.devglan.model.PatientDto;
import com.devglan.service.PatientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

import java.net.URISyntaxException;
import java.util.List;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/patient")
public class PatientController {

    @Autowired
    private PatientService patientService;

    @PostMapping
    public ApiResponse<Patient> savePatient(@RequestBody PatientDto patient){
        return new ApiResponse<>(HttpStatus.OK.value(), "Patient saved successfully.",patientService.save(patient));
    }
    @GetMapping
    public ApiResponse<List<Patient>> listPatient(){
        return new ApiResponse<>(HttpStatus.OK.value(), "Patient list fetched successfully.",patientService.findAll());
    }

    @GetMapping("/{id}")
    public ApiResponse<Patient> getOne(@PathVariable int id){
        return new ApiResponse<>(HttpStatus.OK.value(), "Patient fetched successfully.",patientService.findById(id));
    }

    @PutMapping("/{id}")
    public ApiResponse<PatientDto> update(@RequestBody PatientDto patientDto) {
        return new ApiResponse<>(HttpStatus.OK.value(), "Patient updated successfully.",patientService.update(patientDto));
    }

    @DeleteMapping("/{id}")
    public ApiResponse<Void> delete(@PathVariable int id) {
        patientService.delete(id);
        return new ApiResponse<>(HttpStatus.OK.value(), "Patient deleted successfully.", null);
    }



}
