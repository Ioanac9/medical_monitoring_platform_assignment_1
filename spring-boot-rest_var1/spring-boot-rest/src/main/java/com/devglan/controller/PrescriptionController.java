package com.devglan.controller;

import com.devglan.model.ApiResponse;
import com.devglan.enitites.Prescription;
import com.devglan.model.PrescriptionDto;
import com.devglan.service.PrescriptionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

import java.net.URISyntaxException;
import java.util.List;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/prescription")
public class PrescriptionController {

    @Autowired
    private PrescriptionService prescriptionService;
    @PostMapping
    public ApiResponse<Prescription> savePrescription(@RequestBody PrescriptionDto prescription){
        return new ApiResponse<>(HttpStatus.OK.value(), "Patient saved successfully.",prescriptionService.save(prescription));
    }

    @GetMapping
    public ApiResponse<List<Prescription>> listPrescription(){
        return new ApiResponse<>(HttpStatus.OK.value(), "Prescription list fetched successfully.",prescriptionService.findAll());
    }

//    @GetMapping("/{id}")
//    public ApiResponse<Prescription> findByPatientId(@PathVariable int id){
//        return new ApiResponse<>(HttpStatus.OK.value(), "Prescription fetched successfully.",prescriptionService.findByPatientId(id));
//    }

    @PutMapping("/{id}")
    public ApiResponse<PrescriptionDto> update(@RequestBody PrescriptionDto prescriptionDto) {
        return new ApiResponse<>(HttpStatus.OK.value(), "Prescription updated successfully.",prescriptionService.update(prescriptionDto));
    }

    @DeleteMapping("/{id}")
    public ApiResponse<Void> delete(@PathVariable int id) {
        prescriptionService.delete(id);
        return new ApiResponse<>(HttpStatus.OK.value(), "Prescription deleted successfully.", null);
    }



}
