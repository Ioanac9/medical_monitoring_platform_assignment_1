package com.devglan.enitites;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.Iterator;
import java.util.Set;

@Entity
@Table(name = "patient")
public class Patient {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Column
    private String firstName;
    @Column
    private String lastName;
    @Column
    private LocalDate birthDate;
    @Column
    private String gender;
    @Column
    private String address;
    @Column
    private String medicalRecord;

//    @OneToOne(fetch = FetchType.LAZY, optional = false)
//    @JoinColumn(name = "user_id", nullable = false)
//    @OnDelete(action = OnDeleteAction.CASCADE)
//    @JsonIgnore
//    private Users users;

    @ManyToOne
    @JoinColumn(name="Caregiver_idCaregiver")
    private Caregiver Caregiver_idCaregiver;

//    @OneToMany(mappedBy = "patientId", cascade = CascadeType.ALL,fetch= FetchType.LAZY)
//    private Set<Prescription> prescriptions;

    @OneToMany(mappedBy = "patientId", cascade = CascadeType.ALL,fetch= FetchType.LAZY)
    private Set<Medication> medications;

    @OneToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "user_id", nullable = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    private Users user;

//    public Users getUser() {
//        return user;
//    }


    public void setUser(Users user) {
        this.user = user;
    }

    public void setCaregiver_idCaregiver(Caregiver caregiver_idCaregiver) {
        this.Caregiver_idCaregiver = caregiver_idCaregiver;
    }

    public LocalDate getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(LocalDate birthDate) {
        this.birthDate = birthDate;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getMedicalRecord() {
        return medicalRecord;
    }

    public void setMedicalRecord(String medicalRecord) {
        this.medicalRecord = medicalRecord;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

//    public Set<Prescription> getPrescriptions() {
//        return prescriptions;
//    }
//
//    public void setPrescriptions(Set<Prescription> prescriptions) {
//        this.prescriptions = prescriptions;
//    }

//    public void setMedications(Set<Medication> medications) {
//        this.medications = medications;
//    }

    public Set<Medication> getMedications() {
        return medications;
    }
}
