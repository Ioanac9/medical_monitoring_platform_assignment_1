package com.devglan.enitites;

import java.util.Set;

import javax.persistence.*;

@Entity
@Table(name = "users")
public class Users {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column
    private String username;

    @Column
    private String password;

    @Column
    private String role;

//    @OneToOne(fetch = FetchType.LAZY,
//            cascade =  CascadeType.ALL,
//            mappedBy = "user_id")
//    private Caregiver caregiver;

    @OneToOne(fetch = FetchType.LAZY,
            cascade =  CascadeType.ALL,
            mappedBy = "user")
    private Caregiver caregiver;

    @OneToOne(fetch = FetchType.LAZY,
            cascade =  CascadeType.ALL,
            mappedBy = "user")
    private Patient patient;

    @Column
    private String idCaregiver;

    @Column
    private String idPatient;


    public Patient getPatient() {
        return patient;
    }

    public Caregiver getCaregiver() {
        return this.caregiver;
    }

//    public void setCaregiver(Caregiver caregiver) {
//        this.caregiver = caregiver;
//    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return this.password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getIdCaregiver() {
        if (this.caregiver !=null)
        return String.valueOf(this.caregiver.getId());
        else
            return null;
    }

    public String getIdPatient() {
        if (this.patient !=null)
            return String.valueOf(this.patient.getId());
        else
            return null;
    }
}
