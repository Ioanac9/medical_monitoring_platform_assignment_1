//package com.devglan.enitites;
//
//import com.fasterxml.jackson.annotation.JsonIgnore;
//import org.hibernate.annotations.OnDelete;
//import org.hibernate.annotations.OnDeleteAction;
//
//import javax.persistence.*;
//import java.time.LocalDate;
//import java.util.Set;
//
//@Entity
//@Table(name = "medPlan")
//public class MedPlan {
//
//    @Id
//    @GeneratedValue(strategy = GenerationType.IDENTITY)
//    private int id;
//
//    @OneToOne(fetch = FetchType.LAZY, optional = false)
//    @JoinColumn(name = "patient_id", nullable = false)
//    @OnDelete(action = OnDeleteAction.CASCADE)
//    @JsonIgnore
//    private Patient patient;
//
//    @OneToMany(mappedBy = "medPlan", cascade = CascadeType.ALL,fetch= FetchType.LAZY)
//    private Set<Prescription> prescriptions;
//
//    public Set<Prescription> getPrescriptions() {
//        return prescriptions;
//    }
//
//    public int getId() {
//        return id;
//    }
//
//    public void setId(int id) {
//        this.id = id;
//    }
//
//    public Patient getPatient() {
//        return patient;
//    }
//
//    public void setPatient(Patient patient) {
//        this.patient = patient;
//    }
//
//
//
//    public void setPrescriptions(Set<Prescription> prescriptions) {
//        this.prescriptions = prescriptions;
//    }
//}
