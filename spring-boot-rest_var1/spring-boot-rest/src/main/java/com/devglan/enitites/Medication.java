package com.devglan.enitites;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Table(name = "medication")
public class Medication {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Column
    private String name;
    @Column
    private String sideEffects;
    @Column
    private Integer dosage;

//    @OneToOne(mappedBy = "medication_id")
//    private Prescription prescription;

//    @OneToOne(mappedBy = "medication")
//    private Prescription prescription;

//    public MedPlan getMedPlan() {
//        return medPlan;
//    }

    @OneToOne(fetch = FetchType.LAZY,
            cascade =  CascadeType.ALL,
            mappedBy = "medication")

    private Prescription prescription;

//    public Patient getPatient() {
//        return patient;
//    }

    @ManyToOne
    @JoinColumn(name="patientId")


    private Patient patientId;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSideEffects() {
        return sideEffects;
    }

    public void setSideEffects(String sideEffects) {
        this.sideEffects = sideEffects;
    }

    public Integer getDosage() {
        return dosage;
    }

    public void setDosage(Integer dosage) {
        this.dosage = dosage;
    }

    public Prescription getPrescription() {
        return prescription;
    }

//    public void setPrescription(Prescription prescription) {
//        this.prescription = prescription;
//    }

//    public Patient getPatientId() {
//        return patientId;
//    }

    public void setPatientId(Patient patientId) {
        this.patientId = patientId;
    }
}
