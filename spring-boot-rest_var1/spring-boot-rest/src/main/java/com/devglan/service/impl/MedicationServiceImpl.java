package com.devglan.service.impl;

import com.devglan.dao.MedicationDao;
import com.devglan.enitites.Medication;
import com.devglan.enitites.Patient;
import com.devglan.model.MedicationDto;
import com.devglan.service.MedicationService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


@Service(value = "medicationService")
public class MedicationServiceImpl implements MedicationService {

    @Autowired
    private MedicationDao medicationDao;

    public List<Medication> findAll() {
       // List<Medication> list = new ArrayList<>();
        return medicationDao.findAll();

    }

    @Override
    public void delete(int id) {
        medicationDao.deleteById(id);
    }

    @Override
    public Medication findOne(String name) {
        return medicationDao.findByName(name);
    }

    @Override
    public Medication findById(int id) {
        Optional<Medication> optionalMedication = medicationDao.findById(id);
        return optionalMedication.isPresent() ? optionalMedication.get() : null;
    }

    @Override
    public MedicationDto update(MedicationDto medicationDto) {
        Medication medication = findById(medicationDto.getId());
        if (medication != null) {
            Patient localPatient = new Patient();
            localPatient.setId(medicationDto.getPatientId());
            if(localPatient.getId() != 0){
            medication.setPatientId(localPatient);}
            BeanUtils.copyProperties(medicationDto, medication, "password", "medicationname","prescription","patient");
            medicationDao.save(medication);
        }else{
            BeanUtils.copyProperties(medicationDto, medication, "password", "medicationname","prescription","patient");
        }
        return medicationDto;
    }

    @Override
    public Medication save(MedicationDto medication) {
        Medication newMedication = new Medication();
        newMedication.setName(medication.getName());
        newMedication.setSideEffects(medication.getSideEffects());
        newMedication.setDosage(medication.getDosage());
        newMedication.setPatientId(medication.getPatient());
        return medicationDao.save(newMedication);
    }
}
