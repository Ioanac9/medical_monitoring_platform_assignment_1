package com.devglan.service;

import com.devglan.enitites.Caregiver;
import com.devglan.enitites.Prescription;
import com.devglan.model.PrescriptionDto;

import java.util.List;

public interface PrescriptionService {

    Prescription save(PrescriptionDto prescription);
    List<Prescription> findAll();
    void delete(int id);

    // List<Prescription> findByCaregiver_idCaregiver(Caregiver caregiver_idCaregiver);


    Prescription findById(int id);


    PrescriptionDto update(PrescriptionDto prescriptionDto);

   // List<Prescription> findByPatientId(int patientId);
}
