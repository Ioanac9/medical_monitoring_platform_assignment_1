package com.devglan.service;

import com.devglan.enitites.Caregiver;
import com.devglan.enitites.Patient;
import com.devglan.model.PatientDto;

import java.util.List;

public interface PatientService {

    Patient save(PatientDto patient);
    List<Patient> findAll();
    void delete(int id);

   // List<Patient> findByCaregiver_idCaregiver(Caregiver caregiver_idCaregiver);
    Patient findOne(String firstname);

    Patient findById(int id);

    PatientDto update(PatientDto patientDto);
}
