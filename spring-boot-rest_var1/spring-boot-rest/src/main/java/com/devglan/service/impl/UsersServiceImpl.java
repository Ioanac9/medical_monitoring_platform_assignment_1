package com.devglan.service.impl;

import com.devglan.dao.CaregiverDao;
import com.devglan.dao.UsersDao;
import com.devglan.enitites.Caregiver;
import com.devglan.enitites.Users;
import com.devglan.model.UsersDto;
import com.devglan.service.UsersService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


@Service(value = "usersService")
public class UsersServiceImpl implements UsersService {

    @Autowired
    private UsersDao usersDao;
    private CaregiverDao caregiverDao;

    public List<Users> findAll() {
        List<Users> list = new ArrayList<>();
        //usersDao.findAll().iterator().forEachRemaining(list::add);
        return usersDao.findAll();
    }

    @Override
    public void delete(int id) {
        usersDao.deleteById(id);
    }

    @Override
    public Users findOne(String firstName) {
        return null;
    }

//@PersistenceContext
//private EntityManager entityManager;
//
//    @SuppressWarnings("unchecked")
//    @Override
//    public  List<Users> findByCaregiver_idCaregiver(Caregiver caregiver) {
//
//        return this.entityManager.
//                createQuery("select e from users e where e.users_id like '"+caregiver.getId()+"'").
//                getResultList();
//        List<Users> list = new ArrayList<>();
//
//        usersDao.findByCaregiver(caregiver).iterator().forEachRemaining(list::add);
//
//        Optional<Caregiver> optionalCaregiver = caregiverDao.findById(caregiver.getId());
//        caregiver = optionalCaregiver.isPresent() ? optionalCaregiver.get() : null;
//        if (caregiver != null) {
//            return (List<Users>) usersDao.findByCaregiver(caregiver);
//        }
//        return null;

    //  }


    @Override
    public Users findById(int id) {
        Optional<Users> optionalUsers = usersDao.findById(id);
        return optionalUsers.isPresent() ? optionalUsers.get() : null;
    }

    @Override
    public Users findByUsername(String username,String password) {
        Users findUser =  usersDao.findByUsername(username);
        Users findUser2 =  new Users();
        findUser2.setPassword(findUser.getPassword());
        findUser2.setUsername(findUser.getUsername());


        if (findUser !=null){

            if(password.compareTo(findUser.getPassword())==0)
            {
                return findUser;
            }

        }
        else {
            return null;
        }
     return null;
    }
    @Override
    public  Users findByIdPatient(String idPatient){
        Users findUser =  usersDao.findByIdPatient(idPatient);
        if (findUser !=null){
            return findUser;
        }
        return null;
    }

    @Override
    public UsersDto update(UsersDto usersDto) {
        Users users = findById(usersDto.getId());
        if (users != null) {
            BeanUtils.copyProperties(usersDto, users, "password", "usersname");
            usersDao.save(users);
        }
        return usersDto;
    }

    @Override
    public Users save(UsersDto users) {
        Users newUsers = new Users();
        newUsers.setUsername(users.getUsername());
        newUsers.setPassword(users.getPassword());
        newUsers.setRole(users.getRole());

        return usersDao.save(newUsers);
    }
}
