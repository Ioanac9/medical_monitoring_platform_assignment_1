package com.devglan.service.impl;

import com.devglan.dao.CaregiverDao;
import com.devglan.dao.PatientDao;
import com.devglan.dao.UsersDao;
import com.devglan.enitites.Caregiver;
import com.devglan.enitites.Patient;
import com.devglan.enitites.Users;
import com.devglan.model.PatientDto;
import com.devglan.service.PatientService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


@Service(value = "patientService")
public class PatientServiceImpl implements PatientService {

    @Autowired
    private PatientDao patientDao;

    @Autowired
    private UsersDao usersDao;

    @Autowired
    private CaregiverDao caregiverDao;

    public List<Patient> findAll() {
        List<Patient> list = new ArrayList<>();
        //patientDao.findAll().iterator().forEachRemaining(list::add);
        return patientDao.findAll();
    }

    @Override
    public void delete(int id) {
        patientDao.deleteById(id);
    }

    @Override
    public Patient findOne(String firstName) {
        return patientDao.findByFirstName(firstName);
    }

//@PersistenceContext
//private EntityManager entityManager;
//
//    @SuppressWarnings("unchecked")
//    @Override
//    public  List<Patient> findByCaregiver_idCaregiver(Caregiver caregiver) {
//
//        return this.entityManager.
//                createQuery("select e from patient e where e.patient_id like '"+caregiver.getId()+"'").
//                getResultList();
//        List<Patient> list = new ArrayList<>();
//
//        patientDao.findByCaregiver(caregiver).iterator().forEachRemaining(list::add);
//
//        Optional<Caregiver> optionalCaregiver = caregiverDao.findById(caregiver.getId());
//        caregiver = optionalCaregiver.isPresent() ? optionalCaregiver.get() : null;
//        if (caregiver != null) {
//            return (List<Patient>) patientDao.findByCaregiver(caregiver);
//        }
//        return null;

  //  }


    @Override
    public Patient findById(int id) {
        Optional<Patient> optionalPatient = patientDao.findById(id);
        return optionalPatient.isPresent() ? optionalPatient.get() : null;
    }

    @Override
    public PatientDto update(PatientDto patientDto) {
        Patient patient = findById(patientDto.getId());
        if (patient != null) {
            BeanUtils.copyProperties(patientDto, patient, "patientname","Caregiver_idCaregiver","medications","user");
            patientDao.save(patient);
        }
        return patientDto;
    }

    @Override
    public Patient save(PatientDto patient) {
        Patient newPatient = new Patient();
        newPatient.setFirstName(patient.getFirstName());
        newPatient.setLastName(patient.getLastName());
        newPatient.setBirthDate(patient.getBirthDate());
        newPatient.setGender(patient.getGender());
        newPatient.setAddress(patient.getAddress());
        newPatient.setMedicalRecord(patient.getMedicalRecord());

        Caregiver localCaregiver = new Caregiver ();
        localCaregiver.setId(patient.getCaregiverId());
        localCaregiver.setFirstName(patient.getCaregiverFirstName());
        localCaregiver.setLastName(patient.getCaregiverLastName());
        localCaregiver.setGender(patient.getCaregiverGender());
        localCaregiver.setAddress(patient.getCaregiverAddress());

        Users userNou = new Users();
        // userNou.setUsername(caregiver.getUser().getUsername());
        userNou =  usersDao.findByUsername(patient.getUserUsername());
//        Users userLocal = new Users();
//        userLocal.setId(caregiver.getUser().getId());
        newPatient.setUser(userNou);
        // newCaregiver.setPatientsList(caregiver.getPatients());

        newPatient.setCaregiver_idCaregiver(localCaregiver);

        return patientDao.save(newPatient);
    }
}
