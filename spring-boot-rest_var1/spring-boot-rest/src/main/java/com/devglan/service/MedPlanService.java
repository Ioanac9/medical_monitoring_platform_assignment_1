//package com.devglan.service;
//
//import com.devglan.enitites.MedPlan;
//import com.devglan.model.MedPlanDto;
//
//import java.util.List;
//
//public interface MedPlanService {
//
//    MedPlan save(MedPlanDto medPlan);
//    List<MedPlan> findAll();
//    void delete(int id);
//
//    MedPlan findById(int id);
//
//    MedPlanDto update(MedPlanDto medPlanDto);
//}
