package com.devglan.service;

import com.devglan.enitites.Caregiver;
import com.devglan.enitites.Users;
import com.devglan.model.UsersDto;

import java.util.List;

public interface UsersService {

    Users save(UsersDto users);
    List<Users> findAll();
    void delete(int id);

    // List<Users> findByCaregiver_idCaregiver(Caregiver caregiver_idCaregiver);
    Users findOne(String firstname);

    Users findById(int id);

    Users findByUsername(String username,String password);

    UsersDto update(UsersDto usersDto);

    Users findByIdPatient(String idPatient);
}
