package com.devglan.service;

import com.devglan.enitites.Medication;
import com.devglan.model.MedicationDto;

import java.util.List;

public interface MedicationService {

    Medication save(MedicationDto medication);
    List<Medication> findAll();
    void delete(int id);

    Medication findOne(String firstname);

    Medication findById(int id);

    MedicationDto update(MedicationDto medicationDto);
}
