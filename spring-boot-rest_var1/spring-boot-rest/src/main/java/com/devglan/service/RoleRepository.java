package com.devglan.service;

import com.devglan.enitites.Role;
import org.springframework.data.jpa.repository.JpaRepository;



public interface RoleRepository extends JpaRepository<Role, Integer>{

}
