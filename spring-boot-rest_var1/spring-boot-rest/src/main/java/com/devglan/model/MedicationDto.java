package com.devglan.model;

import com.devglan.enitites.Patient;
import com.devglan.enitites.Prescription;

import javax.persistence.*;

public class MedicationDto {

    private int id;
    private String name;
    private String sideEffects;
    private Integer dosage;
    //private Prescription prescription;
    private Patient patient;
    private int patientId;

    public Patient getPatient() {
        return patient;
    }

    public void setPatient(Patient patient) {
        this.patient = patient;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSideEffects() {
        return sideEffects;
    }

    public void setSideEffects(String sideEffects) {
        this.sideEffects = sideEffects;
    }

    public int getDosage() {
        return dosage;
    }

    public void setDosage(int dosage) {
        this.dosage = dosage;
    }

    public void setDosage(Integer dosage) {
        this.dosage = dosage;
    }

    public int getPatientId() {
        return patientId;
    }

    public void setPatientId(int patientId) {
        this.patientId = patientId;
    }

    //    public Prescription getPrescription() {
//        return prescription;
//    }
//
//    public void setPrescription(Prescription prescription) {
//        this.prescription = prescription;
//    }
}
