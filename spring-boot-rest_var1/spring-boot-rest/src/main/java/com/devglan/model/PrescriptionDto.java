package com.devglan.model;

import com.devglan.enitites.Medication;
import com.devglan.enitites.Patient;

public class PrescriptionDto {
    private int id;
    private Integer intokeInterval;
    private Integer periodTreatment;
    private Medication medication_id;
    private Patient patient;

    private int medicationId;
    private int patientId;

    public int getMedicationId() {
        return medicationId;
    }

    public void setMedicationId(int medicationId) {
        this.medicationId = medicationId;
    }

    public int getPatientId() {
        return patientId;
    }

    public void setPatientId(int patientId) {
        this.patientId = patientId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Patient getPatient() {
        return patient;
    }

    public void setPatient(Patient patient) {
        this.patient = patient;
    }

    public Integer getIntokeInterval() {
        return intokeInterval;
    }

    public void setIntokeInterval(Integer intokeInterval) {
        this.intokeInterval = intokeInterval;
    }

    public Integer getPeriodTreatment() {
        return periodTreatment;
    }

    public void setPeriodTreatment(Integer periodTreatment) {
        this.periodTreatment = periodTreatment;
    }

    public Medication getMedication() {
        return medication_id;
    }

    public void setMedication(Medication medication_id) {
        this.medication_id = medication_id;
    }
}
