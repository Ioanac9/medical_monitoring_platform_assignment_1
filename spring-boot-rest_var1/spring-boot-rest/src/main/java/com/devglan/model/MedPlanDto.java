//package com.devglan.model;
//
//import com.devglan.enitites.Medication;
//import com.devglan.enitites.Patient;
//import com.devglan.enitites.Prescription;
//import com.fasterxml.jackson.annotation.JsonIgnore;
//import org.hibernate.annotations.OnDelete;
//import org.hibernate.annotations.OnDeleteAction;
//
//import javax.persistence.*;
//import java.time.LocalDate;
//import java.util.Set;
//
//public class MedPlanDto {
//
//    private int id;
//    private Patient patient;
//    private Set<Prescription> prescriptions;
//
//    private int patientId;
//    private String patientFirstName;
//    private String patientLastName;
//    private LocalDate patientBirthDate;
//    private String patientGender;
//    private String patientAddress;
//    private String patientMedicalRecord;
//
//    public int getId() {
//        return id;
//    }
//
//    public void setId(int id) {
//        this.id = id;
//    }
//
//    public Patient getPatient() {
//        return patient;
//    }
//
//    public void setPatient(Patient patient) {
//        this.patient = patient;
//    }
//
//    public Set<Prescription> getPrescriptions() {
//        return prescriptions;
//    }
//
//    public void setPrescriptions(Set<Prescription> prescriptions) {
//        this.prescriptions = prescriptions;
//    }
//
//    public int getPatientId() {
//        return patientId;
//    }
//
//    public void setPatientId(int patientId) {
//        this.patientId = patientId;
//    }
//
//    public String getPatientFirstName() {
//        return patientFirstName;
//    }
//
//    public void setPatientFirstName(String patientFirstName) {
//        this.patientFirstName = patientFirstName;
//    }
//
//    public String getPatientLastName() {
//        return patientLastName;
//    }
//
//    public void setPatientLastName(String patientLastName) {
//        this.patientLastName = patientLastName;
//    }
//
//    public LocalDate getPatientBirthDate() {
//        return patientBirthDate;
//    }
//
//    public void setPatientBirthDate(LocalDate patientBirthDate) {
//        this.patientBirthDate = patientBirthDate;
//    }
//
//    public String getPatientGender() {
//        return patientGender;
//    }
//
//    public void setPatientGender(String patientGender) {
//        this.patientGender = patientGender;
//    }
//
//    public String getPatientAddress() {
//        return patientAddress;
//    }
//
//    public void setPatientAddress(String patientAddress) {
//        this.patientAddress = patientAddress;
//    }
//
//    public String getPatientMedicalRecord() {
//        return patientMedicalRecord;
//    }
//
//    public void setPatientMedicalRecord(String patientMedicalRecord) {
//        this.patientMedicalRecord = patientMedicalRecord;
//    }
//}
